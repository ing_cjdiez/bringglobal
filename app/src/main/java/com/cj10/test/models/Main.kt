package com.cj10.test.models

import com.squareup.moshi.Json

data class Main(
    @Json(name = "temp") val temp: Double,
    @Json(name = "humidity") val humidity: Double
)