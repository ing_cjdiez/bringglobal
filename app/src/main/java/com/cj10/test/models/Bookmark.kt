package com.cj10.test.models

data class Bookmark(
    val lat: Double,
    val lng: Double,
    val name: String,
    val date: String
)