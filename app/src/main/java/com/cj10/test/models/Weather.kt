package com.cj10.test.models

import com.squareup.moshi.Json

data class Weather(
    @Json(name = "main") val main: String,
    @Json(name = "description") val description: String
)