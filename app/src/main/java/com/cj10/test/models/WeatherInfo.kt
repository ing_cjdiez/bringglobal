package com.cj10.test.models

import com.squareup.moshi.Json

data class WeatherInfo(
    @Json(name = "coord") val coord: Coord,
    @Json(name = "weather") val weather: List<Weather>,
    @Json(name = "main") val main: Main,
    @Json(name = "wind") val wind: Wind,
    @Json(name = "name") val name: String
)