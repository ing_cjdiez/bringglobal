package com.cj10.test

import android.os.Bundle
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.cj10.test.common.extensions.activityViewModelProvider
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.models.Bookmark
import com.cj10.test.views.BookmarksFragment
import com.cj10.test.views.BookmarksViewModel
import com.cj10.test.views.HelpFragment
import com.cj10.test.views.WeatherDetailsFragment
import com.cj10.test.views.WeatherInfoViewModel
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity(), OnClickListener {

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AndroidInjection.inject(this)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                when(supportFragmentManager.fragments.last()) {
                    is BookmarksFragment -> finish()
                    else -> showHomeFragment()
                }
            }
        }
        onBackPressedDispatcher.addCallback(this, callback)
        showHomeFragment()
    }

    private fun showHomeFragment() {
        val bookmarksViewModel: BookmarksViewModel = activityViewModelProvider(modelFactory)
        changeFragment(getBookmarksFragment(bookmarksViewModel, this))
    }

    private fun getBookmarksFragment(bookmarksViewModel: BookmarksViewModel, onClickListener: OnClickListener) =
            BookmarksFragment.newInstance(bookmarksViewModel, onClickListener)

    private fun getWeatherDetailsFragment(weatherInfoViewModel: WeatherInfoViewModel, bookmark: Bookmark) =
            WeatherDetailsFragment.newInstance(weatherInfoViewModel, bookmark)

    private fun getHelpFragment() = HelpFragment.newInstance()

    private fun changeFragment(fragment: Fragment){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragment_container, fragment, "books")
            .commit()
    }

    override fun onClick(obj: Any) {
        when(obj) {
            is Bookmark -> {
                val weatherInfoViewModel: WeatherInfoViewModel = activityViewModelProvider(modelFactory)
                changeFragment(getWeatherDetailsFragment(weatherInfoViewModel, obj as Bookmark))
            }
            else -> {
                changeFragment(getHelpFragment())
            }
        }
    }
}