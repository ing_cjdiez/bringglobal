package com.cj10.test.repositories

import com.cj10.test.models.Bookmark
import javax.inject.Inject
import javax.inject.Singleton
import io.reactivex.Observable

@Singleton
class BookmarksRepository @Inject constructor() {

    private val bookmarks: ArrayList<Bookmark> = arrayListOf(
        Bookmark(10.9930665,-74.9092287, "Barranquilla", "Dec 10/20"),
        Bookmark(4.6486259,-74.2482376, "Bogotá", "Dec 11/20"),
        Bookmark(40.4381307,-3.8199649, "Madrid", "Dec 12/20"),
        Bookmark(41.9102411,12.3955698, "Roma", "Dec 13/20"),
        Bookmark(51.5287714,-0.2420253, "Londres", "Dec 14/20"),
        Bookmark(52.3547921,4.7635338, "Amsterdam", "Dec 15/20"),
        Bookmark(35.5062648,138.6458318, "Tokio", "Dec 16/20"),
        Bookmark(38.8937796,-77.1550044, "Washington", "Dec 17/20"),
        Bookmark(-37.0579255,-64.5305659, "Buenos Aires", "Dec 18/20"),
        Bookmark(-33.8473551,133.001564, "Sydney", "Dec 19/20")
    )

    fun getAllBookmarks(): Observable<List<Bookmark>> = Observable.just(bookmarks)

    fun addBookmark(bookmark: Bookmark) {
        bookmarks.add(bookmark)
    }
}