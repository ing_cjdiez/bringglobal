package com.cj10.test.repositories

import com.cj10.test.api.WeatherService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WeatherRepository @Inject constructor(private val weatherService: WeatherService) {
    fun getWeatherInfo(lat: String, lng: String, apikey: String) = weatherService.getWeatherInfo(lat = lat, lng = lng, appid = apikey)
}