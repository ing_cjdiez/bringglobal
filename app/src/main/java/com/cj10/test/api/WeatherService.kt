package com.cj10.test.api

import com.cj10.test.BuildConfig
import com.cj10.test.models.WeatherInfo
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET(BuildConfig.WEATHER_ENDPOINT)
    fun getWeatherInfo(
        @Query("lat") lat: String, @Query("lon") lng: String,
        @Query("appid") appid: String, @Query("units") units: String = "metric"
    ): Observable<WeatherInfo>
}