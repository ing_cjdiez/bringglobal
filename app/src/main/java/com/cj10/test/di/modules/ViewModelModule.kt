package com.cj10.test.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.cj10.test.common.annotations.ViewModelKey
import com.cj10.test.di.factory.ViewModelFactory
import com.cj10.test.views.BookmarksViewModel
import com.cj10.test.views.WeatherInfoViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(BookmarksViewModel::class)
    internal abstract fun bindBookmarksViewModel(viewModel: BookmarksViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WeatherInfoViewModel::class)
    internal abstract fun bindWeatherInfoViewModel(viewModel: WeatherInfoViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}