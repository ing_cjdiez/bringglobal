package com.cj10.test.di.modules

import com.cj10.test.api.WeatherService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
object ApiModule {

    @Provides
    @Singleton
    @JvmStatic
    internal fun provideWeatherService(retrofit: Retrofit): WeatherService{
        return retrofit.create(WeatherService::class.java)
    }
}