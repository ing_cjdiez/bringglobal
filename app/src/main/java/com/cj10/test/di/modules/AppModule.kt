package com.cj10.test.di.modules

import android.content.Context
import com.cj10.test.App
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideContext(application: App): Context {
        return application.applicationContext
    }
}