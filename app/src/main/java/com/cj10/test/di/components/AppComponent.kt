package com.cj10.test.di.components

import com.cj10.test.App
import com.cj10.test.di.modules.ActivityModule
import com.cj10.test.di.modules.ApiModule
import com.cj10.test.di.modules.AppModule
import com.cj10.test.di.modules.NetworkModule
import com.cj10.test.di.modules.SchedulerModule
import com.cj10.test.di.modules.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityModule::class,
        ApiModule::class,
        AppModule::class,
        NetworkModule::class,
        SchedulerModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent: AndroidInjector<App> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<App>()
}