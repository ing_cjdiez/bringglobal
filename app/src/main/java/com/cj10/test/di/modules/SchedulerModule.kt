package com.cj10.test.di.modules

import com.cj10.test.common.annotations.Constants.IO
import com.cj10.test.common.annotations.Constants.UI
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named

@Module
object SchedulerModule {

    @Provides
    @JvmStatic
    @Named(UI)
    internal fun ui(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @JvmStatic
    @Named(IO)
    internal fun io(): Scheduler = Schedulers.io()
}