package com.cj10.test.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.cj10.test.R
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Bookmark
import kotlinx.android.synthetic.main.bookmarks_layout.view.recyclerview_bookmarks
import kotlinx.android.synthetic.main.bookmarks_layout.view.tv_loading

class BookmarksFragment(private val viewModel: BookmarksViewModel, private val onClickListener: OnClickListener): Fragment() {

    private lateinit var binding: View
    private val bookmarksAdapter = BookmarksAdapter(onClickListener)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
        viewModel.fetchBookmarks()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflater.inflate(R.layout.bookmarks_layout, container, false)

        activity?.title = getString(R.string.app_name)

        binding.recyclerview_bookmarks.apply {
            adapter = bookmarksAdapter
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            addItemDecoration(
                DividerItemDecoration(
                        context,
                        DividerItemDecoration.VERTICAL
                )
            )
        }

        setHasOptionsMenu(true)

        return binding
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_help, menu);
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_help_item -> {
                onClickListener.onClick(Unit)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun handleStateChange(state: ListViewState?){
        when(state){
            is ListViewState.Loading -> binding.apply {
                tv_loading.visibility = View.VISIBLE
                binding.recyclerview_bookmarks.visibility = View.GONE
            }
            is ListViewState.HasData<*> -> {
                binding.apply {
                    tv_loading.visibility = View.GONE
                    recyclerview_bookmarks.visibility = View.VISIBLE
                }
                bookmarksAdapter.updateBookmarks((state as ListViewState.HasData<Bookmark>).dataList)
            }
            else -> binding.apply {
                tv_loading.visibility = View.GONE
                binding.recyclerview_bookmarks.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(bookmarksViewModel: BookmarksViewModel, onClickListener: OnClickListener): BookmarksFragment {
            return BookmarksFragment(bookmarksViewModel, onClickListener)
        }
    }
}