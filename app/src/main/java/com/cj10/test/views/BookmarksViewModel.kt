package com.cj10.test.views

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cj10.test.common.annotations.Constants
import com.cj10.test.common.errors.NetworkErrorType
import com.cj10.test.common.interfaces.ApiResponse
import com.cj10.test.common.interfaces.ObservableCallback
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Bookmark
import com.cj10.test.repositories.BookmarksRepository
import io.reactivex.Scheduler
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject
import javax.inject.Named

class BookmarksViewModel @Inject constructor(
    private val bookmarksRepository: BookmarksRepository,
    @Named(Constants.UI) private val uiScheduler: Scheduler,
    @Named(Constants.IO) private val ioScheduler: Scheduler
): ViewModel(), ApiResponse<List<Bookmark>?, Throwable>, ObservableCallback {

    var viewState = MutableLiveData<ListViewState>()

    fun fetchBookmarks() {
        bookmarksRepository.getAllBookmarks()
            .subscribeOn(ioScheduler)
            .timeout(10.toLong(), TimeUnit.SECONDS)
            .observeOn(uiScheduler)
            .subscribe(
                { list -> onResponseReceived(list) },
                { error -> onFailure(error) },
                { onFinish() },
                { onStart() }
            )
    }

    override fun onResponseReceived(response: List<Bookmark>?) {
        var bookmarks = response
        viewState.value = if(bookmarks!!.isNotEmpty()) ListViewState.HasData(bookmarks) else ListViewState.NoData
    }

    override fun onFailure(error: Throwable) {
        val errorType = if (error is TimeoutException) NetworkErrorType.TIMEOUT else NetworkErrorType.UNKNOWN
        viewState.value = ListViewState.Error(errorType)
    }

    override fun onStart() {
        viewState.value = ListViewState.Loading
    }

    override fun onFinish() {
        viewState.value = ListViewState.FinishLoading
    }
}