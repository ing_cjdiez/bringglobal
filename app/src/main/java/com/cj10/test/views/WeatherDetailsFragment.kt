package com.cj10.test.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.cj10.test.R
import com.cj10.test.common.annotations.Constants.apiKey
import com.cj10.test.common.states.ListViewState
import com.cj10.test.models.Bookmark
import com.cj10.test.models.WeatherInfo
import kotlinx.android.synthetic.main.bookmark_details.view.group_weather_info
import kotlinx.android.synthetic.main.bookmark_details.view.textView_description
import kotlinx.android.synthetic.main.bookmark_details.view.textView_city
import kotlinx.android.synthetic.main.bookmark_details.view.textView_humidity
import kotlinx.android.synthetic.main.bookmark_details.view.textView_temperature
import kotlinx.android.synthetic.main.bookmark_details.view.textView_wind_speed
import kotlinx.android.synthetic.main.bookmark_details.view.tv_loading_weather

class WeatherDetailsFragment(private val viewModel: WeatherInfoViewModel, private val bookmark: Bookmark): Fragment() {

    private lateinit var binding: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.viewState.observe(this, Observer { handleStateChange(it) })
        viewModel.getWeatherInfo(
            bookmark.lat.toString(), bookmark.lng.toString(), apiKey
        )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = inflater.inflate(R.layout.bookmark_details, container, false)

        activity?.title = bookmark.name

        return binding
    }

    private fun handleStateChange(state: ListViewState?){
        when(state){
            is ListViewState.Loading -> binding.apply {
                tv_loading_weather.visibility = View.VISIBLE
                group_weather_info.visibility = View.GONE
            }
            is ListViewState.HasSingleData<*> -> {
                val weatherInfo = (state as ListViewState.HasSingleData<WeatherInfo>).data
                binding.apply {
                    textView_temperature.text = "${weatherInfo?.main?.temp?.toInt()}º"
                    textView_description.text = weatherInfo?.weather?.get(0)?.description.capitalize()
                    textView_city.text = weatherInfo?.name
                    textView_humidity.text = "${weatherInfo?.main.humidity.toInt()}%"
                    textView_wind_speed.text = "${weatherInfo?.wind.speed.toInt()}km/h"
                }
            }
            else -> binding.apply {
                tv_loading_weather.visibility = View.GONE
                group_weather_info.visibility = View.VISIBLE
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(weatherInfoViewModel: WeatherInfoViewModel, bookmark: Bookmark): WeatherDetailsFragment {
            return WeatherDetailsFragment(weatherInfoViewModel, bookmark)
        }
    }
}