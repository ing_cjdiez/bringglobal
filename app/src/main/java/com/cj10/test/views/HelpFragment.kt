package com.cj10.test.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cj10.test.R
import kotlinx.android.synthetic.main.help_layout.view.webView_help

class HelpFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = inflater.inflate(R.layout.help_layout, container, false)

        activity?.title = getString(R.string.help)

        binding.webView_help.apply {
            settings.setSupportZoom(true)
            isScrollContainer = false
            settings.javaScriptEnabled = true

            val htmlCode = "<!DOCTYPE html>\n" +
                    "<head>\n" +
                    "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "<body>\n" +
                    "\n" +
                    "<center>\n" +
                    "<h1>Project: Weather App</h1>\n" +
                    "</center>\n" +
                    "<h3>This project is a technical task for Bring Global company.</h3><br/>\n" +
                    "\n" +
                    "Technologies/libraries used:<br/><br/>\n" +
                    "\n" +
                    "<b>MVVM:</b> Model-View-ViewModel.<br/>\n" +
                    "<b>DI</b> Dependency injection. Dagger.<br/>\n" +
                    "Observer/Obsevable.<br/>\n" +
                    "Retrofit. Gson<br/>\n" +
                    "Constraint layout.<br/>\n" +
                    "ViewBinding && Databinding.<br/>\n" +
                    "Language: <b>Kotlin</b><br/><br/>\n" +
                    "<h3>How to use it</h3>\n" +
                    "\n" +
                    "Click on WeatherApp icon application.<br/>\n" +
                    "Check the list of <b>\"bookmarkerd\"</b> locations.<br/>\n" +
                    "Click on any <b>\"bookmarked\"</b> item in the list and check the details screen.<br/>\n" +
                    "Click on Help menu in the top right of the screen and check a webview for help.<br/>\n" +
                    "Note: The main <b>\"bookmarked\"</b> list is a hardcoded list inside the app.<br/><br/>\n" +
                    "\n" +
                    "<h3>Clone a repository</h3>\n" +
                    "git clone https://ing_cjdiez@bitbucket.org/ing_cjdiez/bringglobal.git<br/>\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>"
            loadData(htmlCode, "text/html; charset=utf-8", "UTF-8")
        }

        return binding
    }

    companion object {
        @JvmStatic
        fun newInstance(): HelpFragment {
            return HelpFragment()
        }
    }
}