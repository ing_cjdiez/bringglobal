package com.cj10.test.views

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.models.Bookmark
import javax.inject.Inject

class BookmarkItemViewModel @Inject constructor(private val onClickListener: OnClickListener): ViewModel() {

    val bookmarkName = MutableLiveData<String>()
    val bookmarkDate = MutableLiveData<String>()

    fun bind(fixture: Bookmark){
        bookmarkName.value = fixture.name
        bookmarkDate.value = fixture.date
    }

    fun onClick(bookmark: Bookmark) {
        onClickListener.onClick(bookmark)
    }
}