package com.cj10.test.views

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.cj10.test.R
import com.cj10.test.common.interfaces.OnClickListener
import com.cj10.test.databinding.BookmarkItemBinding
import com.cj10.test.models.Bookmark

class BookmarksAdapter(private val onClickListener: OnClickListener): RecyclerView.Adapter<BookmarksAdapter.ViewHolder>() {

    var bookmarksList: ArrayList<Bookmark> = ArrayList()
    private lateinit var binding: BookmarkItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.bookmark_item, parent, false)
        return ViewHolder(binding, onClickListener)
    }

    override fun getItemCount(): Int {
        return bookmarksList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.bind(bookmarksList[pos])
    }

    fun updateBookmarks(bookmarks: List<Bookmark>){
        bookmarksList.clear()
        bookmarksList.addAll(bookmarks)
        notifyDataSetChanged()
    }

    class ViewHolder(private val bookmarkItemBinding: BookmarkItemBinding, private val onClickListener: OnClickListener):
            RecyclerView.ViewHolder(bookmarkItemBinding.root){

        private val viewModel = BookmarkItemViewModel(onClickListener)

        fun bind(bookmark: Bookmark){
            viewModel.bind(bookmark)
            bookmarkItemBinding.viewModel = viewModel
            bookmarkItemBinding.bookmark = bookmark
        }
    }
}