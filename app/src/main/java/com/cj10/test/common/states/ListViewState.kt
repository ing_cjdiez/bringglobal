package com.cj10.test.common.states

import com.cj10.test.common.errors.NetworkErrorType
import com.cj10.test.common.interfaces.State

sealed class ListViewState : State {
    object Loading : ListViewState()
    object FinishLoading : ListViewState()
    object NoData : ListViewState()
    class HasData<T>(var dataList: List<T>) : ListViewState()
    class HasSingleData<T>(var data: T) : ListViewState()
    class Error(var networkErrorType: NetworkErrorType) : ListViewState()
}