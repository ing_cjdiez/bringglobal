package com.cj10.test.common.interfaces

interface ApiResponse<T, E>{
    fun onResponseReceived(response: T)
    fun onFailure(error: E)
}