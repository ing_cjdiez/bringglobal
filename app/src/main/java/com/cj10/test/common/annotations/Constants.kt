package com.cj10.test.common.annotations

object Constants {
    const val UI = "ui"
    const val IO = "io"

    const val apiKey = "c6e381d8c7ff98f0fee43775817cf6ad"
    const val helpUrl = "https://bitbucket.org/ing_cjdiez/bringglobal/src/master/"
}