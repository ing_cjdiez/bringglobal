package com.cj10.test.common.interfaces

interface ObservableCallback {
    fun onStart()
    fun onFinish()
}