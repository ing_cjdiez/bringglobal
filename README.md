---

## Project: Weather App

This project is a technical task for Bring Global company.

Technologies/libraries used:

1. **MVVM**: Model-View-ViewModel.
2. **DI** Dependency injection. Dagger.
3. Observer/Obsevable.
4. Retrofit. Gson
5. Constraint layout.
6. ViewBinding && Databinding.
7. Language: **Kotlin**

---

## How to use it

1. Click on **WeatherApp** icon application.
2. Check the list of "bookmarkerd" locations.
3. Click on any "bookmarked" item in the list and check the details screen.
4. Click on **Help** menu in the top right of the screen and check a webview for help.

**Note:** The main "bookmarked" list is a hardcoded list inside the app.

---

## Clone a repository

git clone https://ing_cjdiez@bitbucket.org/ing_cjdiez/bringglobal.git
